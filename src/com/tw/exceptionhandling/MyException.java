package com.tw.exceptionhandling;

public class MyException extends Exception {
    public MyException(int ans) {
        super("MyException" + "[" + ans + "]");
    }
}
