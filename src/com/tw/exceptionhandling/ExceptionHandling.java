package com.tw.exceptionhandling;

import java.util.Scanner;

public class ExceptionHandling {

    static void solve(int[] arr) throws Exception {

        int result = 0;
        for (int index = 0; index < 10; index++)
            for (int nextToIndex = index + 1; nextToIndex < 10; nextToIndex++)
                result += arr[index] / arr[nextToIndex];

        throw new MyException(result);
    }

    public static void main(String[] args) {

        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter number of elements");
            int numberOfElements = scanner.nextInt();
            int[] arr = new int[numberOfElements];
            scanner.nextLine();

            System.out.println("Enter elements: ");
            for (int index = 0; index < 10; index++)
                arr[index] = Integer.parseInt(scanner.nextLine());

            System.out.println("Enter string");
            String string = scanner.nextLine();
            System.out.println(string.charAt(10));

            solve(arr);

        } catch (ArithmeticException arithmeticException) {
            System.out.println("Invalid division");
        } catch (StringIndexOutOfBoundsException stringIndexOutOfBoundsException) {
            System.out.println("Index is invalid");
        } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
            System.out.println("Array index is invalid");
        } catch (NumberFormatException numberFormatException) {
            System.out.println("Format mismatch");
        } catch (MyException myException) {
            System.out.println(myException.getMessage());
        } catch (Exception exception) {
            System.out.println("Exception encountered");
        } finally {
            System.out.println("Exception Handling Completed");
        }
    }
}

